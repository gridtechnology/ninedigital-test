const express = require('express');
const AppService = require('../services/app.service');

const apiRouter = express.Router();

// middleware that is specific to this router
apiRouter.use((req, res, next) => {
    console.log('Time: ', Date.now());

    // Setting rules here

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    //res.setHeader('Access-Control-Allow-Credentials', true);

    next();
})

// handle GET
apiRouter.get('/', (req, res) => {
    AppService.root(req, res);
})

// handle POST
apiRouter.post('/', (req, res) => {
    AppService.root(req, res);
})

module.exports = apiRouter;
