/*
 */

const responseHandler = require('../utils/responseHandler');

class appService {
    constructor() {
        this._data = undefined;
        this._responseHandler = responseHandler;
    }

    // request handler
    root (req, res) {
        this._data = req.body;
        this._res = res;

        let responseData = this.filter(this._data);

        this._responseHandler.success(responseData, this._res);
    }

    filter(data) {
        let payloads = data.payload;

        if (!payloads || payloads.length === 0) {
            return undefined;
        }

        /*  Filter Data
         *  Conditions:
         *    - 1. drm === true
         *    - 2. episodeCount > 0
         */
        payloads = payloads
        .filter(playload => playload.drm && playload.episodeCount > 0)
        .map( playload => {
            return {
                image: playload.image.showImage,
                slug: playload.slug,
                title: playload.title
            }
        });

        return payloads;
    }
}

const AppService = new appService();

module.exports = AppService;
