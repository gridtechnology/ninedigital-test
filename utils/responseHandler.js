class responseHandler {
    constructor() {
        this._errorLists = {
            400 : 'Could not decode request: JSON parsing failed'
        }
    }

    success(data, res) {
        res.status(200);
        res.send({
            response : data
        });
    }

    error(code, res) {
      res.status(code || 500);
      res.send({
          error: this._errorLists[code] || "Sorry, system error!"
      });
    }
}

const ResponseHandler = new responseHandler();

module.exports = ResponseHandler;
