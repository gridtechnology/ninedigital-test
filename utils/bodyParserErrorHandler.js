const responseHandler = require('../utils/responseHandler');

const clientErrorHandler = (error, req, res, next) => {
    if (error && error instanceof SyntaxError) {
        responseHandler.error(400, res);
        return;
    }

    next();
};

const bodyParserErrorHandler = () => {
    return clientErrorHandler;
};

module.exports = bodyParserErrorHandler;
