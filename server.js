
const express = require('express');
const bodyParser = require('body-parser');
const compression = require('compression');

const apiRouter = require('./routers/app.router');
const config = require('./utils/app.config');
const bodyParserErrorHandler = require('./utils/bodyParserErrorHandler');
const app = express();

app.use(compression());
app.use(bodyParser.json());
app.use(bodyParserErrorHandler());

app.use('/', apiRouter);

app.listen(process.env.PORT || config.port);
console.log(`Server listening on port ${config.port}`);
